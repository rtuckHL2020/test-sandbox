@IsTest
private class Test_HL_OpportunityApprovalHandler {
    @isTest private static void TestInsertOpportunityApprovalWithNoRelatedEngagements(){
        Opportunity__c opp = SL_TestSetupUtils.CreateOpp('',1)[0];
        Opp.Beneficial_Owner_Control_Person_form__c = 'No';
        insert opp;
        Opportunity_Approval__c oa = SL_TestSetupUtils.CreateOA('',1)[0];
        oa.Related_Opportunity__c = opp.id;
        
         //Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(oa);
        Test.stopTest();
        
        System.Assert(result.isSuccess());
    }
    
    @isTest private static void TestInsertOpportunityApprovalWithRelatedEngagements(){
        //Setup the Data
        //Use this as our subject account
        Account a = SL_TestSetupUtils.CreateAccount('',1)[0];
        insert a;
        List<Opportunity__c> o = SL_TestSetupUtils.CreateOpp('',2);
        o[0].Beneficial_Owner_Control_Person_form__c = 'No';
        o[1].Beneficial_Owner_Control_Person_form__c = 'No';
        
        insert o;
        List<Engagement__c> eList = SL_TestSetupUtils.CreateEngagement('',5);
        insert eList;
        Opportunity_Approval__c oa =  SL_TestSetupUtils.CreateOA('',1)[0];
        oa.Related_Opportunity__c = o[0].id;
        Opportunity_Approval__c oaBuyside =  SL_TestSetupUtils.CreateOA('',1)[0];
        oaBuyside.Related_Opportunity__c = o[1].id;
        
        for(Engagement__c e : eList)
            e.Subject__c = a.Id;
        
        eList[0].Job_Type__c = 'Buyside';
        
        for(Integer i = 0; i<o.size(); i++)
        {
            o[i].Subject__c = a.Id;
            if(i == 1)
                o[i].Job_Type__c = 'Buyside';
        }
        
        update o;
        update eList;
        
        //Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(oa);
        Database.SaveResult resultBuyside = Database.insert(oaBuyside);
        Test.stopTest();
        
        //Verify insert was successful
        System.Assert(result.isSuccess());
        System.Assert(resultBuyside.isSuccess());
        //Verify the Previous Transaction field is not null
        oa = [SELECT Id, Previous_Transactions__c FROM Opportunity_Approval__c WHERE Id =:result.getId()];
        System.Assert(oa.Previous_Transactions__c <> null); 
        oa = [SELECT Id, Previous_Transactions__c FROM Opportunity_Approval__c WHERE Id =:resultBuyside.getId()];
        System.Assert(oa.Previous_Transactions__c <> null); 
    }
}