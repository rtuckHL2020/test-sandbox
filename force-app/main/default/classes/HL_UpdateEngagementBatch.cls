global class HL_UpdateEngagementBatch implements Database.Batchable<Engagement__c>{
List<Engagement__c> lstEngagement = new List<Engagement__c>();
global HL_UpdateEngagementBatch(List<Engagement__c> lstEngagement){
    this.lstEngagement = lstEngagement;
}
global Iterable<Engagement__c>start(Database.BatchableContext BC){
return lstEngagement;
}
global void execute(Database.BatchableContext BC,List<Engagement__c> lstEngagement){
//update lstEngagement;

//Block the Engagement Trigger from Firing
    HL_TriggerContextUtility.ByPassOnMonthlyRevenueProcess = true;
    update lstEngagement;
    //Unblock Engagement Trigger from Firing
    HL_TriggerContextUtility.ByPassOnMonthlyRevenueProcess = false;
}
global void finish(Database.BatchableContext BC){


}

}