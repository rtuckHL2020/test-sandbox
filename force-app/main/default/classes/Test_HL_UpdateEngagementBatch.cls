@isTest
private class Test_HL_UpdateEngagementBatch{
    
    private static testmethod void testEngUpdateBatch(){
        Account clientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert clientAccount;
        
        Account subjectAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert subjectAccount;
        
        //FAS Engagements
        List<Engagement__c> eFAS_List = SL_TestSetupUtils.CreateEngagement('', 2);
        for(Engagement__c e : eFAS_List){
            e.Name='Test FAS Engagement';
            e.Client__c = clientAccount.Id;
            e.Subject__c = subjectAccount.Id;
            e.Line_of_Business__c = 'FAS';
            e.Job_Type__c = 'FMV Transaction Based Opinion';
            e.Primary_Office__c = 'NY';
            e.Total_Estimated_Fee__c = 100000;
            e.RecordTypeId = '012i0000001NDwh';
        }
        //Create one with 0% completed to Update to 50%
        Engagement__c eFAS_Stage50 = eFAS_List[0];
        eFAS_Stage50.Stage__c = 'Retained';
        
        //Create one with 75% completed to Update to 100%
        Engagement__c eFAS_Stage100 = eFAS_List[1];
        eFAS_Stage100.Stage__c = 'Opinion Report';
        
        insert eFAS_Stage50;
        insert eFAS_Stage100;
        
        Test.startTest();
        
        List<Engagement__c> lstEngToUpdate = new List<Engagement__c>();
        //Change the Stage to 50% Complete
        eFAS_Stage50.Stage__c = 'Performing Analysis';
        lstEngToUpdate.add(eFAS_Stage50);
            Database.Executebatch(new HL_UpdateEngagementBatch(lstEngToUpdate));
        Test.stopTest();
        //Change the Stage to 100% Complete
        eFAS_Stage100.Final_Report_Sent_Date__c = Date.today();
        eFAS_Stage100.Stage__c = 'Bill/File';
        update eFAS_Stage100;
        
    }
    private static testmethod void testAsyncMonthlyProcessQueue(){
        Account clientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert clientAccount;
        
        Account subjectAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert subjectAccount;
        
        //FAS Engagements
        List<Engagement__c> eFAS_List = SL_TestSetupUtils.CreateEngagement('', 2);
        for(Engagement__c e : eFAS_List){
            e.Name='Test FAS Engagement';
            e.Client__c = clientAccount.Id;
            e.Subject__c = subjectAccount.Id;
            e.Line_of_Business__c = 'FAS';
            e.Job_Type__c = 'FMV Transaction Based Opinion';
            e.Primary_Office__c = 'NY';
            e.Total_Estimated_Fee__c = 100000;
            e.RecordTypeId = '012i0000001NDwh';
        }
        
        
        Test.startTest();
        
        System.enqueueJob(new HL_AsyncMonthlyProcessControl(eFAS_List, true));
        
        Test.stopTest();
        
        
    }
 }