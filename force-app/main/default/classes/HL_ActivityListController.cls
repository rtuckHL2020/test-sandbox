//Publicis Sapient - Class updated for Activity Home page Change. also modified HL_ActivityList vf component.
//Publicis Sapient - Replaced 'My' FilterText with 'All'

public without sharing class HL_ActivityListController extends HL_ActivityControllerBase {
    public static final integer PageSize = 5;
    // PS - All activity will be shown as default on all place where this controller used .
    public String FilterText {get {return String.isBlank(filterText) ? (EntityType == 'User' ? 'All' : 'All') : filterText;} set;}
    public String SortDirection {get{return sortDirection == null ? 'ASC' : sortDirection;} set;}
    public Boolean isValidToAccessContactComment{get;set;}
    public Integer StartIndex {get{
        if (startIndex == null)
            startIndex = 0;
        return startIndex;
    } set;
                              }
    public Integer EndIndex {get{
        if (endIndex == null)
            endIndex = PageSize;
        return endIndex;
    } set;
                            }
    public Integer TotalRecords {get; set;}
    public Integer TotalPages {get{
        if (totalPages == null)
            totalPages = 0;
        return totalPages;
    } set;
                              }
    public Integer Page {get {
        if (page == null)
            page = 1;
        return page;
    } set;
                        }
    public Boolean HasPrevious {
        get{return Page > 1;}
    }
    public Boolean HasNext {
        get{return Page < TotalPages;}
    }
    public String NewSort {get; set;}
    
    public String SortField {get{return sortField == null ? 'StartDateTime DESC, CreatedDate' : sortField;} set{
        ToggleDirection(value);
        SortField = value;
    }
                            }
    
    private Set<Id> EntityContacts {get{
        if (entityContacts == null) {
            entityContacts = new Set<Id>();
            //For Accounts, We Need to Get the Account Contacts
            if (EntityType == 'Account') {
                for (Contact c : HL_Contact.GetByCompany(EntityId))
                    entityContacts.add(c.Id);
            }
            //For User, We Need to Get The User's Contact Record
            else if (EntityType == 'User')
                entityContacts.add(HL_Contact.GetByUserId(EntityId).Id);
        }
        return entityContacts;
    } set;
                                   }
    
    public HL_ActivityListController() {
        if (ApexPages.currentPage() != null)
            EntityId =  HL_PageUtility.GetParameter('id');
        TotalRecords = 0;
    }
    
    public HL_ActivityListController(ApexPages.StandardController controller) {
        EntityId = HL_PageUtility.GetParameter('id');
        TotalRecords = 0;
        isValidToAccessContactComment = HL_ContactCommentHandler.isValidToCreateContactComment();
    }
    
    public void PreviousPage() {
        Page = Page - 1;
    }
    public void NextPage() {
        Page = Page + 1;
    }
    
    public void FilterAll() {
        FilterText = 'All';
    }
    public void FilterToday() {
        FilterText = 'Today\'s';
    }
    public void FilterUpcoming() {
        FilterText = 'Upcoming';
    }
    
    private String GetWhereClause() {          
        string whereClause = '(WhatId=:EntityId OR WhoId=:EntityId) ';
        if (EntityType == 'Account')
            whereClause = '(WhatId=:EntityId OR WhoId IN : contactIds) ';
        else if (EntityType == 'User')
            whereClause = 'WhoId IN : contactIds AND Mask_As_External__c = false ';
        else {
            HL_ActivityListController con = new HL_ActivityListController();
            BuildMenteeMentorSets();
            //if (!con.HasInternalAccess && !menteeIdSet.contains(UserContactRecord.id) && !mentorIdSet.contains(UserContactRecord.id) && !hasMentorActivityAccess)
            if (!con.HasInternalAccess)
                whereClause += 'AND Mask_As_External__c = false ';
        }
        whereClause += FilterText == 'Today\'s' ? 'AND StartDateTime = TODAY ' : FilterText == 'Upcoming' ? 'AND StartDateTime > TODAY ' : '';
        return whereClause;
    }
    Set<Id> menteeIdSet = new Set<Id>();
    Set<Id> mentorIdSet = new Set<Id>();
    private void BuildMenteeMentorSets() {     
        
        for(Event e : [SELECT ParentId__c,Id,Mask_As_External__c, WhoId FROM Event WHERE (WhatId=:EntityId OR WhoId=:EntityId) ]){
            if(e.Mask_As_External__c)
                menteeIdSet.add(e.WhoId); 
            else
                mentorIdSet.add(e.whoId);                
        }    
    }
    public void ToggleDirection(string newSort) {
        SortDirection = (SortField == newSort) ? (SortDirection == 'DESC' ? 'ASC' : 'DESC') : 'ASC';
    }
    
    private List<HL_Activity> GetActivityList() {
        List<HL_Activity> aList = new List<HL_Activity>();
        Set<Id> activityIds = new Set<Id>();
        Set<Id> contactIds = this.EntityContacts;
        List<Contact> primaryAttendees = new List<Contact>(); //Store the Primary Attendees so that we can create a User Map for Permissions
        List<Event> eventList = new List<Event>(); //Store the final event list to convert to HL_Activity Records
        Id userId = UserInfo.getUserId();        
        String modifiedWhereClause =  ' (Type != \'Internal Mentor Meeting\' AND '+ GetWhereClause() +') OR ((WhatId=:EntityId OR WhoId=:EntityId) AND Type = \'Internal Mentor Meeting\') '; 
          
     //   for (Event e : Database.Query(HL_Event.STANDARD_SELECT + 'FROM Event WHERE ' + GetWhereClause() + ' ORDER BY ' + SortField + ' ' + SortDirection + ', ParentId__c LIMIT 1000')) {
          for (Event e : Database.Query(HL_Event.STANDARD_SELECT + 'FROM Event WHERE ' + modifiedWhereClause + ' ORDER BY ' + SortField + ' ' + SortDirection + ', ParentId__c LIMIT 2000')) {           
            //Prevent Duplicates when Parent and Child Records from Same Place are Assigned (i.e. Contact at Company Discussed)
            if (e.ParentId__c <> null && !activityIds.contains(e.ParentId__c)) {
                //Capture Records Within the Target Range
                if (TotalRecords >= StartIndex && TotalRecords < EndIndex) {
                    eventList.Add(e);
                    if (!String.isBlank(e.Primary_Attendee_Id__c))
                        primaryAttendees.add(new Contact(Id = e.Primary_Attendee_Id__c));
                    activityIds.Add(e.Id);
                    activityIds.Add(e.ParentId__c);
                }
                //Count the Total Number of Records
                TotalRecords++;
            } 
        }
        
        //Get a Map of Event Id with HL Attendees
        Map<Id, Set<Id>> eventToHLAttendeeMap = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> eventToHLMenteeMap = new Map<Id, Set<Id>>();
        system.debug('--activityIds---'+activityIds);
        for(Event e : [SELECT ParentId__c, WhoId, Mask_As_External__c FROM Event WHERE ParentId__c IN: activityIds AND WhoID <> null]){
            if(!e.Mask_As_External__c)
            { 
                if(eventToHLAttendeeMap.get(e.ParentId__c) == null)
                    eventToHLAttendeeMap.put(e.ParentId__c, new Set<Id>{e.WhoId});
                else{
                    Set<Id> attendeeSet = eventToHLAttendeeMap.get(e.ParentId__c);
                    attendeeSet.add(e.WhoId);
                    eventToHLAttendeeMap.put(e.ParentId__c, attendeeSet);
                }   
            }
            else
            {
                if(eventToHLMenteeMap.get(e.ParentId__c) == null){
                    eventToHLMenteeMap.put(e.ParentId__c, new Set<Id>{e.WhoId});
                }
                else{
                    Set<Id> menteeSet = eventToHLMenteeMap.get(e.ParentId__c);
                    menteeSet.add(e.WhoId);
                    eventToHLMenteeMap.put(e.ParentId__c, menteeSet);
                }   
            }
        }
        Map<Id, Contact> primaryAttendeeMap = HL_Contact.GetMap(primaryAttendees);
        system.debug('---'+eventToHLAttendeeMap);
        for (Event e : eventList) {
            HL_Activity a ; 
            if(e.Type == 'Internal Mentor Meeting'){
                 a = this.CreateActivityFromEvent(UserContactRecord.Id, userId, e, primaryAttendeeMap, eventToHLAttendeeMap.get(e.ParentId__c), eventToHLMenteeMap.get(e.ParentId__c));}
            else{
                 a = this.CreateActivityFromEvent(UserContactRecord.Id, userId, e, primaryAttendeeMap, eventToHLAttendeeMap.get(e.ParentId__c) , null);
            }
            aList.Add(a);
        }
        return aList;
    }
    
    @AuraEnabled
    public static List<Event> GetMyActivities(string filter) {
        Contact contactRecord = HL_Contact.GetByUserId();
        Id contactId = contactRecord.Id;
        string filterClause = filter == 'Today' ? 'AND StartDateTime = TODAY ' : filter == 'Upcoming' ? 'AND StartDateTime > TODAY ' : '';
        
        filterClause += GetInternalFilterClause();
        
        return GetActivityList(HL_Event.STANDARD_SELECT +
                                      'FROM Event ' +
                                      'WHERE WhoId = \'' + contactId + '\' ' + filterClause +
                                      'ORDER BY StartDateTime DESC, CreatedDate ASC, ParentId__c ' +
                                      'LIMIT 50');
    }
    
    @AuraEnabled
    public static List<Event> GetMyActivitiesByDate(String selectedDate) {
        Contact contactRecord = HL_Contact.GetByUserId();
        Date paramDate = Date.valueOf(selectedDate.subString(0, 10));
        Id contactId = contactRecord.Id;
        string filterClause = '';
        
        filterClause += GetInternalFilterClause();
        
        return GetActivityList(HL_Event.STANDARD_SELECT +
                                      'FROM Event ' +
                                      'WHERE WhoId = \'' + contactId + '\' AND Mask_As_External__c = false AND Start_Date__c = ' + selectedDate.subString(0, 10) + ' ' + filterClause +
                                      'ORDER BY StartDateTime DESC, CreatedDate ASC, ParentId__c ' +
                                      'LIMIT 50');
    }
    
    private static List<Event> GetActivityList(string query){
        Set<Id> activityIdSet = new Set<Id>();
        List<Event> activityList = new List<Event>();
        
         for (Event e : Database.query(query)) 
         {
             if (!activityIdSet.contains(e.Id) && (e.ParentId__c == null || !activityIdSet.contains(e.ParentId__c))) {
                 activityList.add(e);
                 activityIdSet.Add(e.Id);
                 activityIdSet.Add(e.ParentId__c);
             }
        }
        
        return activityList;
    }
    
    private static string GetInternalFilterClause(){
        HL_ActivityListController con = new HL_ActivityListController();
        string filterClause = '';
        
        if (!con.HasInternalAccess)
            filterClause = ' AND Type != \'Internal\' ';
        
        if(!con.HasInternalSupervisorAccess)
            filterClause += ' AND Mask_As_External__c = false ';
        //Added By Harsh for Internal Mentor Meeting         
        if(!con.hasMentorActivityAccess)
            filterClause = ' AND Type != \'Internal Mentor Meeting\' ';
            
        return filterClause;
    }
    
    public List<HL_Activity> GetActivities() {
        List<HL_Activity> aList = new List<HL_Activity>();
        
        //Determine the Range of Records to Return
        StartIndex = (Page - 1) * PageSize;
        EndIndex = Page * PageSize;
        TotalRecords = 0;
        
        //Iterate over the Events to create the Activity List
        aList = this.GetActivityList();
        
        //Calculate total pages
        Decimal pages = Decimal.valueOf(TotalRecords);
        pages = pages.divide(Decimal.valueOf(pageSize), 2);
        TotalPages = (Integer)pages.round(System.RoundingMode.CEILING);
        
        //Adjust the Start Index
        StartIndex++;
        
        //Adjust End Index
        if (EndIndex > TotalRecords)
            EndIndex = TotalRecords;
        
        
        return aList;
    }
    
    public void ApplySort() {
        SortField = NewSort;
    }
}