@isTest private class Test_HL_TimeRecordHandler {
    @isTest
    private static void TestAfterInsertRecordCreation() {
        final integer RECORDS_TO_INSERT = 10;

        Test.startTest();

        List<Time_Record__c> timeRecordList = SL_TestSetupUtils.CreateTimeRecordsWithRelated(RECORDS_TO_INSERT, 'Litigation');

        Test.stopTest();

        //Validate the time records were created
        System.assert([SELECT Id FROM Time_Record__c].size() == RECORDS_TO_INSERT);
        //Validate the the Time Record Rollup Day records were created
        System.assert([SELECT Id FROM Time_Record_Rollup_Day__c].size() > 0);
    }

    @isTest
    private static void TestAfterRecordUpdate() {
        final integer RECORDS_TO_INSERT = 10;
        final integer HOURS_WORKED = 2;

        List<Time_Record__c> timeRecordList = SL_TestSetupUtils.CreateTimeRecordsWithRelated(RECORDS_TO_INSERT, 'Litigation');
        for (Time_Record__c tr : timeRecordList)
            tr.Hours_Worked__c = HOURS_WORKED;

        Test.startTest();

        update timeRecordList;

        Test.stopTest();

        //Validate the rollup record was adjusted
        AggregateResult ar = [SELECT SUM(Hours_Worked__c) hours FROM Time_Record_Rollup_Day__c];
        System.assertEquals(RECORDS_TO_INSERT * HOURS_WORKED, ar.get('hours'));
    }

    @isTest
    private static void TestAfterProjectUpdate() {
        final integer RECORDS_TO_INSERT = 10;
        List<Time_Record__c> timeRecordList = SL_TestSetupUtils.CreateTimeRecordsWithRelated(RECORDS_TO_INSERT, 'Litigation');
        Opportunity__c opp = SL_TestSetupUtils.CreateOpp('', 1)[0];
        opp.Beneficial_Owner_Control_Person_form__c = 'No';
        insert opp;

        for (Time_Record__c tr : timeRecordList) {
            tr.Activity_Date__c = Date.today();
            tr.Opportunity__c = opp.Id;
            tr.Hours_Worked__c = 1;
        }

        Test.startTest();

        update timeRecordList;

        Test.stopTest();

        //Validate there is only one rollup record
        System.assertEquals(1, [SELECT Id FROM Time_Record_Rollup_Day__c].size());
    }

    @isTest
    private static void TestAfterRecordDelete() {
        final integer RECORDS_TO_INSERT = 10;
        final integer HOURS_WORKED = 2;

        List<Time_Record__c> timeRecordList = SL_TestSetupUtils.CreateTimeRecordsWithRelated(RECORDS_TO_INSERT, 'Litigation');
        for (Time_Record__c tr : timeRecordList) 
            tr.Hours_Worked__c = HOURS_WORKED;
        update timeRecordList;

        Test.startTest();

        delete timeRecordList;

        Test.stopTest();

        //Validate the rollup record was adjusted
        AggregateResult ar = [SELECT SUM(Hours_Worked__c) hours FROM Time_Record_Rollup_Day__c];
        System.assertEquals(NULL, ar.get('hours'));
    }
}