({
    doInit: function(component, event, helper) {
		helper.doInit(component, helper);
	},
	onActivityRecordChanged: function(component, event, helper) {
        var activityRecord = component.get("v.activityRecord");
        activityRecord.Hours = helper.round(activityRecord.Hours, 1);
        component.set("v.activityRecord", activityRecord);
		helper.onActivityRecordChanged(component, helper);
	},
    onDeleteClicked: function(component, event, helper) {
        if (confirm('Are you sure you want to delete this record?')){
            var deleteControl = event.getSource();
   			var recordId = deleteControl.getElement().parentElement.getAttribute("data-key");
        	helper.deleteActivityRecord(component, helper, recordId);
        }
    }
})