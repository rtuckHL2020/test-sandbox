trigger HL_InvestmentTrigger on Investment__c (before insert, after insert, before update, after update) {
    //Enable/Disable trigger setting
    if(HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Investment)){
        //Creating instance of handler class
        HL_InvestmentHandler handler = new HL_InvestmentHandler(Trigger.isExecuting, Trigger.size);
        if(Trigger.isInsert){

            if(Trigger.isBefore){
                handler.OnBeforeInsert(Trigger.new);
            }
        
            if(Trigger.isAfter){
                handler.OnAfterInsert(Trigger.newMap);
            }
        }
        if(Trigger.isUpdate){
            if(Trigger.isBefore){
                handler.OnBeforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
            }
        }
    }    
}