trigger HL_ContractTrigger on Contract__c (before insert, before update, before delete, after insert, after update, after delete){
    
    if(HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Contract)){
        if(trigger.isBefore){
            if(trigger.isInsert){
               HL_ContractHandler.setNewContractTaxClassificationCode();
               HL_ContractHandler.setBillToShipToBasedOnBillingContact(Trigger.New,Null,True);
               HL_ContractHandler.validateContractBillingContact(Trigger.New,Null,True);
            }
            else if(trigger.isUpdate){
              HL_ContractHandler.setBillToShipToBasedOnBillingContact(Trigger.New,Trigger.OldMap,False);
              HL_ContractHandler.validateContractBillingContact(Trigger.New,Trigger.OldMap,False);
            }
            else if(trigger.isDelete){
            }
        }
        else{
            if(trigger.isInsert){
                HL_ContractHandler.setContractInitialValues();
                HL_ContractHandler.updateAccountClientNumbers();
            }
            else if(trigger.isUpdate){
                HL_ContractHandler.updateAccountClientNumbers();
            }
            else if(trigger.isDelete){
            }
        }
    }
}