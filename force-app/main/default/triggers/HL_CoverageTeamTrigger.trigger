trigger HL_CoverageTeamTrigger on Coverage_Team__c (before update, before delete, after insert, after update, after delete) {
    if(Trigger.isAfter)
    {
        if(Trigger.isDelete){
            HL_CoverageTeamHandler.UpdateCoverageTeamAggregate(Trigger.Old);
        }
        else{
            HL_CoverageTeamHandler.UpdateCoverageTeamAggregate(Trigger.New);
        }
    }

    //Audit Tracking
    if(Trigger.IsBefore && (Trigger.IsUpdate || Trigger.IsDelete) &&
            HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Audit_Coverage_Team)){
        HL_AuditRecordHandler auditHandler = new HL_AuditRecordHandler(SObjectType.Coverage_Team__c.getSobjectType());
        auditHandler.RecordAudit(Trigger.oldMap, Trigger.newMap);
    }
}